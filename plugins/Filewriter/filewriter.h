#ifndef FILEWRITER_H
#define FILEWRITER_H

#include <QObject>
#include <QStandardPaths>

class Filewriter: public QObject {
    Q_OBJECT

public:
    Filewriter();
    ~Filewriter() = default;

    Q_INVOKABLE void speak();

    Q_INVOKABLE QString configPath();
    Q_INVOKABLE QString cachePath();
    Q_INVOKABLE QString appDataPath();
};


#endif
