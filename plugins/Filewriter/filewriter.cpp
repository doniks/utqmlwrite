#include <QDebug>
#include <QStandardPaths>
#include <QFile>
#include <QDir>
#include <QDateTime>
#include "filewriter.h"

Filewriter::Filewriter() {

}

void Filewriter::speak() {
    qDebug() << "hello world!"
        << configPath()
        << cachePath()
        << appDataPath();

    auto now = QDateTime::currentDateTime();

    QDir dir(appDataPath());
    if (!dir.exists())
        dir.mkpath(".");

    QString filename=appDataPath() + "/" + "Data.txt";
    QFile file( filename );
    if ( file.open(QIODevice::ReadWrite) ){
        QTextStream stream( &file );
        stream << "appdata "
            << now.toString() << endl;
    }else {
        qWarning() << "failed to write to" << filename << "[" << file.error() << "]";
    }

    filename="/home/phablet/HData.txt";
    file.close();
    file.setFileName(filename);
    if ( file.open(QIODevice::ReadWrite) ){
        QTextStream stream( &file );
        stream << "home "
            << now.toString() << endl;


    }else {
        qWarning() << "failed to write to" << filename << "[" << file.error() << "]";
    }

    filename="/home/phablet/Videos/VData.txt";
    file.close();
    file.setFileName(filename);
    if ( file.open(QIODevice::ReadWrite) ){
        QTextStream stream( &file );
        stream << "video "
            << now.toString() << endl;
    }else {
        qWarning() << "failed to write to" << filename << "[" << file.error() << "]";
    }
}


QString Filewriter::configPath()
{
    return QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation);
}
QString Filewriter::cachePath()
{
    return QStandardPaths::writableLocation(QStandardPaths::CacheLocation);
}
QString Filewriter::appDataPath()
{
    return QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
}
