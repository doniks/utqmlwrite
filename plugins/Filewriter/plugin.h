#ifndef FILEWRITERPLUGIN_H
#define FILEWRITERPLUGIN_H

#include <QQmlExtensionPlugin>

class FilewriterPlugin : public QQmlExtensionPlugin {
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QQmlExtensionInterface")

public:
    void registerTypes(const char *uri);
};

#endif
