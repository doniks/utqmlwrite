import QtQuick 2.4
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3
import Filewriter 1.0

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'writer.doniks'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    Page {
        id: page
        anchors.fill: parent

        header: PageHeader {
            id: header
            title: i18n.tr('Write a file')
        }

        Column{
            anchors.top: header.bottom
            anchors.left: page.left
            anchors.right: page.right
            anchors.bottom: page.bottom
            Label {
                anchors.horizontalCenter: parent.horizontalCenter
                text: i18n.tr('Hello World!')
            }
            Label {
                anchors.horizontalCenter: parent.horizontalCenter
                text: Filewriter.configPath()
            }
        }
    }

    Component.onCompleted: Filewriter.speak()
}
